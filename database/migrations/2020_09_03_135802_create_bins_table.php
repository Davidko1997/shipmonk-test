<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBinsTable extends Migration
{
    private \App\Values\Bins\Bins $bins;

    public function __construct(\App\Values\Bins\Bins $bins)
    {
        $this->bins = $bins;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bins', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        /** @var \App\Dto\BinDto $bin */
        foreach ($this->bins->getBins() as $bin){
            DB::table('roles')->insert([
                'name' => $bin
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bins');
    }
}
