<?php

namespace App\Exceptions;

use Exception;

class CalculationApiException extends Exception
{
    private array $options;

    public function __construct(string $message, int $code = 0, Exception $previous = null, array $options = ['params'])
    {
        parent::__construct($message, $code, $previous);

        $this->options = $options;
    }

    public function GetOptions(): array
    {
        return $this->options;
    }
}
