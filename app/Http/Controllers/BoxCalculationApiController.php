<?php

namespace App\Http\Controllers;


use App\Dto\BinDto;
use App\Dto\ItemDto;
use App\Exceptions\CalculationApiException;
use App\Http\Requests\BoxCalculationRequest;
use App\services\CalculationService;
use App\Values\Bins\Bins;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;

class BoxCalculationApiController extends Controller
{
    private CalculationService $calculationService;

    private Bins

    public function __construct(CalculationService $calculationService, Bins $bins)
    {
        $this->calculationService = $calculationService;
        $this->bins = $bins;
    }

    /**
     * @throws \ReflectionException
     * @throws CalculationApiException
     * @throws GuzzleException
     */
    public function index(BoxCalculationRequest $request): JsonResponse
    {
        $items = [];

        foreach ($request->data as $item) {
            $items[] = new ItemDto($item['width'], $item["height"], $item["length"], $item["weight"]);
        }

       $bin = $this->calculationService->calculateItems($items, $this->bins->getBins());

        return new JsonResponse($bin);
    }
}
