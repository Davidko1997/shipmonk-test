<?php

namespace App\Http\Requests;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class BoxCalculationRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'data' => 'present|array',
            'data.*.width' => 'required|numeric|gt:0',
            'data.*.height' => 'required|numeric|gt:0',
            'data.*.length' => 'required|numeric|gt:0',
            'data.*.weight' => 'required|numeric|gt:0',
        ];
    }

    /**
     * @throws HttpResponseException
     */
    public function failedValidation(Validator $validator): void
    {
        $errors = $validator->errors();

        throw new HttpResponseException(new JsonResponse([
            'errors' => $errors->messages()
        ]));
    }
}
