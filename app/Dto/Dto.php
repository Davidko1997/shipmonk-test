<?php


namespace App\Dto;


use ReflectionException;

class Dto
{
    /**
     * @throws ReflectionException
     */
    public function getValues(): array
    {
        $data = [];

        foreach ((new \ReflectionClass($this))->getProperties() as $property) {
            $property->setAccessible(true);
            $data[$property->getName()] = $property->getValue($this);
        }

        return $data;
    }
}
