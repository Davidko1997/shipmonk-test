<?php


namespace App\Dto;


class BinDto extends Dto
{
    private string $id;

    private float $width;

    private float $height;

    private float $length;

    private float $maxWeight;

    public function __construct(string $id, float $width, float $height, float $length, float $maxWeight)
    {
        $this->id = $id;
        $this->width = $width;
        $this->height = $height;
        $this->length = $length;
        $this->maxWeight = $maxWeight;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getWidth(): float
    {
        return $this->width;
    }

    public function getHeight(): float
    {
        return $this->height;
    }

    public function getLength(): float
    {
        return $this->length;
    }

    public function getMaxWeight(): float
    {
        return $this->maxWeight;
    }
}
