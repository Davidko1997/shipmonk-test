<?php


namespace App\Dto;


use App\Exceptions\CalculationApiException;

trait ValidateInstancesOfDto
{
    /**
     * @throws \ReflectionException
     * @throws CalculationApiException
     */
    private function validateInstances(array $items, string $nameClass): array
    {
        $validatedData = [];

        /** @var BinDto $item */
        foreach ($items as $item) {
            if (!$item instanceof $nameClass) {
                throw new CalculationApiException("Bins must be instances of $nameClass class.");
            }

            $validatedData [] = $item->getValues();
        }

        return $validatedData;
    }
}
