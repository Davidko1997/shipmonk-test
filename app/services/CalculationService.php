<?php


namespace App\services;


use App\Bin;
use App\Combination;
use App\Dto\BinDto;
use App\Dto\ItemDto;
use App\Dto\ValidateInstancesOfDto;
use App\Exceptions\CalculationApiException;
use App\Product;

class CalculationService
{
    use ValidateInstancesOfDto;

    private CalculationApiService $calculationApi;

    private CacheService $cacheService;

    public function __construct(CalculationApiService $calculationApi, CacheService $cacheService)
    {
        $this->calculationApi = $calculationApi;
        $this->cacheService = $cacheService;
    }

    private function createCombinations($data): Bin
    {
        foreach ($data['binsPacked'] as  $binPacked) {
            // @TODO vytvorit novu kombinaciu prejst kazdy balik a ulozit ho do kombinacie a tu namapovat na bin
        }
    }

    /**
     * @param ItemDto $items[]
     * @param BinDto $bins[]
     * @throws CalculationApiException
     * @throws \ReflectionException
     */
    public function calculateItems(array $items, array $bins): Bin
    {
        $binsValidated = $this->validateInstances($bins, BinDto::class);
        $itemsValidated = $this->validateInstances($items, ItemDto::class);

        $bin = $this->cacheService->getCachedBin($itemsValidated);

        if ($bin) {
            return $bin;
        }

        $data = $this->calculationApi->call($itemsValidated, $binsValidated);

        return $this->createCombinations($data);
    }
}
