<?php


namespace App\services;



use App\Dto\BinDto;
use App\Dto\ItemDto;
use App\Dto\ValidateInstancesOfDto;
use App\Exceptions\CalculationApiException;
use GuzzleHttp\Client;

class CalculationApiService
{
    use ValidateInstancesOfDto;

    const USER_NAME = "simple";

    const API_KEY = "a49b174e1e73bf3b5034e1201d360960";

    const API_URL = "http://eu.api.3dbinpacking.com/packer/pack?query={}";

    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @throws CalculationApiException
     */
    public function call(array $items, array $bins): array
    {
        $response = $this->client->post(self::API_URL, [
            'bins' => $bins,
            'items' => $items,
            'username' => self::USER_NAME,
            'api_key' => self::API_KEY,
            'params' => []
        ]);

        $response = json_decode($response->getBody(), false, 512, JSON_THROW_ON_ERROR);

        if (count($response->response->errors)) {
            throw new CalculationApiException("Calculation api throw some errors check the options.",
                0, null, $response->response->errors);
        }

        return [
            'binsPacked' => $response->response->bins_packed,
            'notPackedItems' => $response->response->not_packed_items
        ];
    }
}
