<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['width', 'height', 'length', 'weight', 'bin_id'];
}
