<?php


namespace App\Values\Bins;


use App\Dto\BinDto;
use App\Exceptions\BinsException;

class Bins
{
    private array $bins;

    public function __construct()
    {
        $this->bins = [
            'bin1'=> (new BinDto('bin1', 400, 300, 200, 500)),
            'bin2'=> (new BinDto('bin2', 400, 300, 200, 500)),
            'bin3'=> (new BinDto('bin3', 400, 300, 200, 500)),
            'bin4'=> (new BinDto('bin4', 400, 300, 200, 500)),
        ];
    }

    public function getById(string $id): BinDto
    {
        if (!array_key_exists($id, $this->bins)) {
            throw new BinsException("Bin with id $id, doesn't exist.");
        }

        return $this->bins[$id];
    }

    public function getBins(): array
    {
        return $this->bins;
    }
}
